

export interface ResError extends Error {
    status?: number;
}