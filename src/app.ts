import cookieParser from 'cookie-parser';
import express from 'express';
import log from './logger';
import { catch404, errorHandler, requestLog } from './middleware';
import { initDatabase } from './database';
import userRouter from './api/echo';
import fs from 'fs';

if (!fs.existsSync('./public')) {
  fs.mkdirSync('./public', {recursive: true})
}

let app = express();

app.use(requestLog)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('./public'));

app.use(userRouter);

app.use(catch404);
app.use(errorHandler);

initDatabase();

let port = process.env.PORT ?? 3000;

app.listen(port, () => {
  log('info')('Server running on port %s', port);
})
