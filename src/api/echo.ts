import express from 'express';

const userRouter = express.Router();

userRouter.get('/api/echo', function(req, res, next) {
    res.send(req.query);
});

userRouter.post('/api/echo', function(req, res, next) {
    res.send(req.body);
});

export default userRouter